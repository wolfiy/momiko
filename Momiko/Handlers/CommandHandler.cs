﻿using Discord.Commands;
using Discord.WebSocket;
using System.Reflection;

namespace Momiko.Handlers
{
    /// <summary>
    /// Allows the bot to respond to commands.
    /// </summary>
    public class CommandHandler
    {
        private const char PREFIX = '!';
        private readonly DiscordSocketClient _client;
        private readonly CommandService _service;

        public CommandHandler(DiscordSocketClient client) 
        {
            _client = client;

            // Adding all commands / modules
            _service = new CommandService();
            _service.AddModulesAsync(Assembly.GetEntryAssembly(), null);

            _client.MessageReceived += HandleCommandAsync;
        }

        private async Task HandleCommandAsync(SocketMessage sm)
        {
            // Returns null if cast is invalid
            var message = sm as SocketUserMessage;
            if (message == null) return;

            // Getting command
            var context = new SocketCommandContext(_client, message);
            int argPos = 0;
            if (message.HasCharPrefix(PREFIX, ref argPos))
            {
                var result = await _service.ExecuteAsync(context, argPos, null);

                // Silent wrong commands
                if (!result.IsSuccess)
                {
                    await context.Channel.SendMessageAsync(result.ErrorReason);
                }
            }
        }
    }
}
