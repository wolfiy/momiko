﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Momiko.Handlers;

namespace Momiko
{
    public class Momiko
    {
        private const string TOKEN = "D:\\token.txt";
        private DiscordSocketClient _client;
        private CommandHandler _commandHandler;

        public async Task MainAsync()
        {
            // Intents
            var config = new DiscordSocketConfig()
            {
                GatewayIntents = GatewayIntents.All
            };

            // Creating the client
            _client = new DiscordSocketClient(config);
            _client.Log += Log;

            _commandHandler = new CommandHandler(_client);
            // Connection to Discord
            string token = File.ReadAllText(TOKEN);
            await _client.LoginAsync(TokenType.Bot, token);
            await _client.StartAsync();

            // Prevents the program from closing itself
            await Task.Delay(-1);
        }

        /// <summary>
        /// Handles Discord .NET logs.
        /// </summary>
        /// <param name="message">A log message.</param>
        /// <returns></returns>
        private Task Log(LogMessage message)
        {
            Console.WriteLine(message.ToString());
            return Task.CompletedTask;
        }
    }
}
