﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Momiko.Modules
{
    public class TestModule : ModuleBase<SocketCommandContext>
    {
        [Command("Ping")]
        public async Task Ping()
        {
            await Context.Channel.SendMessageAsync("Pong!");
        }

        [Command("Say")]
        public async Task Say(string message)
        {
            await Context.Channel.SendMessageAsync(message);
        }
    }
}
