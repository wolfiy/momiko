﻿using System;
using System.Threading.Tasks;

namespace Momiko
{
    internal class Program
    {
        static void Main(string[] args) => new Momiko().MainAsync()
                                                       .GetAwaiter()
                                                       .GetResult();
    }
}